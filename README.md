# Bomberman API

<div align="center">
<img width="300" height="300" src="api.png">
</div>

## Description du projet

Bibliothèque réalisée avec Java SE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Programmation réseaux" durant l'année 2019-2020. \
Elle permet de rendre interopérable une application afin de communiquer avec d'autres systèmes.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'unversité d'Angers :
- Benoît DA MOTA : benoit.da-mota@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Programmation réseaux" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2020 sur la période du confinement COVID-19 à la maison au mois de Mars-Avril. \
Il a été terminé et rendu le 09/04/2020.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Maven ;
- API REST ;
- JSON.

## Objectif

Permettre de convertir des objets java en format texte et inversement grâce au JSON.