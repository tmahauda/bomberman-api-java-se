package universite.angers.master.info.api.translater;

import universite.angers.master.info.api.converter.Convertable;

/**
 * Classe qui permet de traduire un message T vers K et inversement
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class Translater<T, K> implements Translable<T, K> {
	
	/**
	 * Convertisseur qui permet de convertir un message K vers un message T
	 */
	private Convertable<T, K> convertMessageKToT;
	
	/**
	 * Convertisseur qui permet de convertir un message T vers un message K
	 */
	private Convertable<K, T> convertMessageTToK;
	
	public Translater(Convertable<T, K> convertMessageKToT, Convertable<K, T> convertMessageTToK) {
		this.convertMessageKToT = convertMessageKToT;
		this.convertMessageTToK = convertMessageTToK;
	}
	
	public synchronized T translateMessageKToT(K message) {
		return this.convertMessageKToT.convert(message);
	}
	
	public synchronized K translateMessageTToK(T message) {
		return this.convertMessageTToK.convert(message);
	}

	/**
	 * @return the convertMessageKToT
	 */
	public Convertable<T, K> getConvertMessageKToT() {
		return convertMessageKToT;
	}

	/**
	 * @param convertMessageKToT the convertMessageKToT to set
	 */
	public void setConvertMessageKToT(Convertable<T, K> convertMessageKToT) {
		this.convertMessageKToT = convertMessageKToT;
	}

	/**
	 * @return the convertMessageTToK
	 */
	public Convertable<K, T> getConvertMessageTToK() {
		return convertMessageTToK;
	}

	/**
	 * @param convertMessageTToK the convertMessageTToK to set
	 */
	public void setConvertMessageTToK(Convertable<K, T> convertMessageTToK) {
		this.convertMessageTToK = convertMessageTToK;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((convertMessageKToT == null) ? 0 : convertMessageKToT.hashCode());
		result = prime * result + ((convertMessageTToK == null) ? 0 : convertMessageTToK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Translater<?, ?> other = (Translater<?, ?>) obj;
		if (convertMessageKToT == null) {
			if (other.convertMessageKToT != null)
				return false;
		} else if (!convertMessageKToT.equals(other.convertMessageKToT))
			return false;
		if (convertMessageTToK == null) {
			if (other.convertMessageTToK != null)
				return false;
		} else if (!convertMessageTToK.equals(other.convertMessageTToK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Translater [convertMessageKToT=" + convertMessageKToT + ", convertMessageTToK=" + convertMessageTToK
				+ "]";
	}
}
