package universite.angers.master.info.api.translater.json;

import java.util.Collection;

import universite.angers.master.info.api.converter.json.ConverterCollectionObjectToJson;
import universite.angers.master.info.api.converter.json.ConverterJsonToCollectionObject;
import universite.angers.master.info.api.translater.Translater;

/**
 * Classe qui permet de traduire une collection d'objet en json et inversement
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class TranslaterCollectionObjectToJson<T> extends Translater<Collection<T>, String> {

	public TranslaterCollectionObjectToJson(ConverterJsonToCollectionObject<T> converterJsonToCollectionObject, 
			ConverterCollectionObjectToJson<T> converterCollectionObjectToJson) {
		super(converterJsonToCollectionObject, converterCollectionObjectToJson);
	}
}
