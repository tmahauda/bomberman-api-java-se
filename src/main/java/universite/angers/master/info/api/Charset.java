package universite.angers.master.info.api;

/**
 * Enumération de l'encodage possible des caractères
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public enum Charset {
	
	UTF_8("utf-8");
	
	/**
	 * Nom de l'encodage
	 */
	private String name;
	
	private Charset(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
