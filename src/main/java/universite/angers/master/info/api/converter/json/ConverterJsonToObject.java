package universite.angers.master.info.api.converter.json;

import org.apache.log4j.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.converter.Convertable;
import universite.angers.master.info.api.util.APIUtil;

/**
 * Interface qui permet de traduire un objet T en une chaine JSON
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ConverterJsonToObject<T> implements Convertable<T, String> {

	private static final Logger LOG = Logger.getLogger(ConverterJsonToObject.class);
	
	/**
	 * Token qui permet de récupérer le type concret de la classe à convertir afin de récupérer ses attributs
	 */
	private TypeToken<T> typeObject;
	
	public ConverterJsonToObject(TypeToken<T> typeObject) {
		this.typeObject = typeObject;
	}
	
	@Override
	public T convert(String serialize) {
		LOG.debug("Serialize : " + serialize);
		if(APIUtil.isNullOrEmpty(serialize)) return null;

		Gson gson = new GsonBuilder()
				.setPrettyPrinting()
				.create();
		try {
			return gson.fromJson(serialize, this.typeObject.getType());
		} 
		catch(Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * @return the typeObject
	 */
	public TypeToken<T> getTypeObject() {
		return typeObject;
	}

	/**
	 * @param typeObject the typeObject to set
	 */
	public void setTypeObject(TypeToken<T> typeObject) {
		this.typeObject = typeObject;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((typeObject == null) ? 0 : typeObject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConverterJsonToObject<?> other = (ConverterJsonToObject<?>) obj;
		if (typeObject == null) {
			if (other.typeObject != null)
				return false;
		} else if (!typeObject.equals(other.typeObject))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ConverterJsonToObject [typeObject=" + typeObject + "]";
	}
}
