package universite.angers.master.info.api.translater.json;

import java.util.Collection;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.converter.json.ConverterCollectionObjectToJson;
import universite.angers.master.info.api.converter.json.ConverterJsonToCollectionObject;
import universite.angers.master.info.api.converter.json.ConverterJsonToObject;
import universite.angers.master.info.api.converter.json.ConverterObjectToJson;

/**
 * Classe qui permet de construire un traducteur JSON
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class TranslaterJsonFactory {

	private TranslaterJsonFactory() {
		
	}
	
	//CONVERSION OBJECT --> JSON
	
	/**
	 * Traducteur qui permet de traduire un objet vers du json
	 * @param <T>
	 * @param typeToken
	 * @return
	 */
	public static <T> TranslaterObjectToJson<T> getTranslaterObjectToJson(TypeToken<T> typeToken) {
		if(typeToken == null) return null;
		
		ConverterJsonToObject<T> converterJsonToObject = new ConverterJsonToObject<>(typeToken);
		ConverterObjectToJson<T> converterObjectToJson = new ConverterObjectToJson<>(typeToken);
		
		return new TranslaterObjectToJson<>(converterJsonToObject, converterObjectToJson);
	}
	
	/**
	 * Traducteur qui permet de traduire une collection d'objets vers du json
	 * @param <T>
	 * @param typeToken
	 * @return
	 */
	public static <T> TranslaterCollectionObjectToJson<T> getTranslaterCollectionObjectToJson(TypeToken<Collection<T>> typeToken) {
		if(typeToken == null) return null;
		
		ConverterJsonToCollectionObject<T> converterJsonToCollectionObject = new ConverterJsonToCollectionObject<>(typeToken);
		ConverterCollectionObjectToJson<T> converterCollectionObjectToJson = new ConverterCollectionObjectToJson<>(typeToken);
		
		return new TranslaterCollectionObjectToJson<>(converterJsonToCollectionObject, converterCollectionObjectToJson);
	}
	
	//CONVERSION JSON --> OBJECT
	
	/**
	 * Traducteur qui permet de traduire du json vers un objet
	 * @param <T>
	 * @param typeToken
	 * @return
	 */
	public static <T> TranslaterJsonToObject<T> getTranslaterJsonToObject(TypeToken<T> typeToken) {
		if(typeToken == null) return null;
		
		ConverterJsonToObject<T> converterJsonToObject = new ConverterJsonToObject<>(typeToken);
		ConverterObjectToJson<T> converterObjectToJson = new ConverterObjectToJson<>(typeToken);
		
		return new TranslaterJsonToObject<>(converterObjectToJson, converterJsonToObject);
	}
	
	/**
	 * Traducteur qui permet de traduire du json vers une collection d'objets
	 * @param <T>
	 * @param typeToken
	 * @return
	 */
	public static <T> TranslaterJsonToCollectionObject<T> getTranslaterJsonToCollectionObject(TypeToken<Collection<T>> typeToken) {
		if(typeToken == null) return null;
		
		ConverterJsonToCollectionObject<T> converterJsonToObject = new ConverterJsonToCollectionObject<>(typeToken);
		ConverterCollectionObjectToJson<T> converterObjectToJson = new ConverterCollectionObjectToJson<>(typeToken);
		
		return new TranslaterJsonToCollectionObject<>(converterObjectToJson, converterJsonToObject);
	}
}
