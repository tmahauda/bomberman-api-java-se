package universite.angers.master.info.api.translater.json;

import universite.angers.master.info.api.converter.json.ConverterJsonToObject;
import universite.angers.master.info.api.converter.json.ConverterObjectToJson;
import universite.angers.master.info.api.translater.Translater;

/**
 * Classe qui permet de traduire un objet en json et inversement
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class TranslaterJsonToObject<T> extends Translater<String, T> {

	public TranslaterJsonToObject(ConverterObjectToJson<T> converterObjectToJson, 
			ConverterJsonToObject<T> converterJsonToObject) {
		super(converterObjectToJson, converterJsonToObject);
	}
}
