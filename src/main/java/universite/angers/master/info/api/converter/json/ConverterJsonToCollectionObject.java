package universite.angers.master.info.api.converter.json;

import java.util.Collection;
import java.util.Collections;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.converter.Convertable;
import universite.angers.master.info.api.util.APIUtil;

/**
 * Interface qui permet de traduire un objet T en une chaine JSON
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ConverterJsonToCollectionObject<T> implements Convertable<Collection<T>, String> {

	private static final Logger LOG = Logger.getLogger(ConverterJsonToCollectionObject.class);
	
	/**
	 * Token qui permet de récupérer le type concret de la classe à convertir afin de récupérer ses attributs
	 */
	private TypeToken<Collection<T>> typeCollectionObject;
	
	public ConverterJsonToCollectionObject(TypeToken<Collection<T>> typeCollectionObject) {
		this.typeCollectionObject = typeCollectionObject;
	}
	
	public Collection<T> convert(String serialize) {
		LOG.debug("Serialize : " + serialize);
		if(APIUtil.isNullOrEmpty(serialize)) return Collections.emptyList();
		
		Gson gson = new GsonBuilder()
				.setPrettyPrinting()
				.create();
		try {
			return gson.fromJson(serialize, this.typeCollectionObject.getType());
		} 
		catch(Exception e) {
			LOG.error(e.getMessage(), e);
			return Collections.emptyList();
		}
	}

	/**
	 * @return the typeCollectionObject
	 */
	public TypeToken<Collection<T>> getTypeCollectionObject() {
		return typeCollectionObject;
	}

	/**
	 * @param typeCollectionObject the typeCollectionObject to set
	 */
	public void setTypeCollectionObject(TypeToken<Collection<T>> typeCollectionObject) {
		this.typeCollectionObject = typeCollectionObject;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((typeCollectionObject == null) ? 0 : typeCollectionObject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConverterJsonToCollectionObject<?> other = (ConverterJsonToCollectionObject<?>) obj;
		if (typeCollectionObject == null) {
			if (other.typeCollectionObject != null)
				return false;
		} else if (!typeCollectionObject.equals(other.typeCollectionObject))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ConverterJsonToCollectionObject [typeCollectionObject=" + typeCollectionObject + "]";
	}
}
