package universite.angers.master.info.api.converter;

import org.apache.log4j.Logger;

/**
 * Interface qui permet de traduire une chaine vers une autre chaine
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ConverterStringToString implements Convertable<String, String> {

	private static final Logger LOG = Logger.getLogger(ConverterStringToString.class);
	
	@Override
	public String convert(String message) {
		LOG.debug("Message to convert : " + message);
		return message;
	}
}
